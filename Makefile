CFLAGS = -Wall
DEBUG = -g
CC = gcc
INCLUDE = -Iinclude/
H_DIR = include/
C_DIR = src/
O_DIR = obj/
BIN = bin/
EXEC = karaoke
SOURCES = $(wildcard $(C_DIR)*.c)
OBJETS = $(patsubst $(C_DIR)%.c, $(O_DIR)%.o, $(SOURCES))
VERSION = v0.1
TARNAME = Karaoke-$(VERSION).tar.xz
MODE = CAIRO


ifneq ($(MODE),TEXT)
CFLAGS += -lcairo -lm -lX11
INCLUDE += -I/usr/include/cairo
endif

ifeq ($(MODE),TEXT)
CFLAGS += -D$(MODE)
endif



$(BIN)$(EXEC): $(OBJETS)
	@echo "-- LINKING OBJECTS --"
	@mkdir -p $(BIN)
	@$(CC) $(CFLAGS) $(INCLUDE) $^ -o $@

$(O_DIR)$(EXEC).o: $(C_DIR)$(EXEC).c   # Added because karaoke.h doesn't exist
	@echo "Compiling $@"
	@mkdir -p $(O_DIR)
	@$(CC) $(CFLAGS) $(DEBUG) $(INCLUDE) -c $< -o $@

$(O_DIR)%.o: $(C_DIR)%.c $(H_DIR)%.h
	@echo "Compiling $@"
	@mkdir -p $(O_DIR)
	@$(CC) $(CFLAGS) $(DEBUG) $(INCLUDE) -c $< -o $@


clean:
	rm -fr $(BIN) $(O_DIR) $(TARNAME)

dist:
	tar -cv --lzma $(C_DIR) $(H_DIR) Makefile README.md -f $(TARNAME)
