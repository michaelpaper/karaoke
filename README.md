# Karaoke

## Goal

The idea of this project is to display a subtitles file (in the [Enhanced LRC format](https://en.wikipedia.org/wiki/LRC_(file_format))) describing the lyrics of a song.

This display should be available in at least the three following formats :
 [x] Command-line
 [ ] py3status bar
 [ ] X11 Window


## Instructions

To compile the program, simply type the following command in your terminal :
```bash
make [MODE=TEXT]
```
Then, you might want to [obtain an "OAuth Token" from Spotify](https://developer.spotify.com/console/get-search-item/) and to update it in `scripts/sp.sh`, line 215, right after "Bearer", at the end of the line.
To run the program in your terminal, use the following syntax :
```bash
bin/karaoke <path-to-lrc-file>
```

## Lyrics database

Lyrics are not provided with this project, but some lyrics are available. To obtain them, you will have to clone [this repo](https://gitlab.com/HommeViande/lyrics). I advise you to clone it at the root of this project, so you can execute the programm by simply typing the following in your terminal :
```bash
bin/karaoke lyrics/<Name Of Your Song>.lrc
```


## Author

Michael - @HommeViande - PAPER : `michael.paper@protonmail.com`

## Licence

This project is licensed under the GPL license, see the [LICENSE](LICENSE) file for details.
