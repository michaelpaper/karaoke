#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "lrc_info.h"
#include "lyrics.h"

void handle_segv(int sig)
{
  if (sig == SIGSEGV)
    {
      fprintf(stderr, "Wow so that was a shitty gift... Seg fault (core dumped)\n");
      exit(1);
    }
}



int main(int argc, char** argv)
{
  signal(SIGSEGV, handle_segv);
  FILE * lyrics = NULL;
  if (argc != 2)
    {
      fprintf(stderr, "syntax : karaoke <path-to-lrc-file>");
      exit(1);
    }
  if (!(lyrics = fopen(argv[1], "r")))
    {
      fprintf(stderr, "File not found\n");
      exit(1);
    }
  char artist[255], album[255], title[255], command[511];
  get_album(lyrics, album);
  get_artist(lyrics, artist);
  get_title(lyrics, title);
  sprintf(command, "scripts/sp.sh search %s", title);
  printf("%s\n", command);
  system(command);
  printf("Artiste : %s\n", artist);
  printf("Album : %s\n", album);
  printf("Title : %s\n", title);
  song s;
  s = init_song(lyrics);
  display(s);
  free(s.text);
  return 0;
}
